/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Button,
  TouchableOpacity
} from 'react-native';

class App extends Component{


    constructor(props){
      super(props);
      this.state={
        email:""
      }
    }


    validate_field=()=>{
      let reg = /^(?=.{1,64}@)[A-Za-z0-9_-]+(\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,4})$/;

      const {email}=this.state
      if(reg.test(email) === false){
        alert("Not a valid email")
      }else{
        alert("email is valid")
      }

    };


    render(){
      return(

        <View style={styles.containerView}>
            <TextInput style={styles.inputFields}
             onChangeText={value => this.setState({ email: value })}
             placeholder="Email" />
             <Button
              onPress={this.validate_field}
              title="Test"
              style={styles.testButton}
              
          />
        </View>
      )
    }
}

const styles= StyleSheet.create({
  containerView: {
    flex: 1,
    width:'100%',
    height:'100%',
    alignSelf:'center',
    alignContent:'center',
    alignItems:'center',
    justifyContent: 'center',
    
  },
  
  inputFields: {
    fontSize: 20,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'gray',
    borderRadius: 5,
    marginBottom: 15,
    width:'80%'
  },
  testButton: {
    backgroundColor: '#34A853'
  }
});

export default App;
